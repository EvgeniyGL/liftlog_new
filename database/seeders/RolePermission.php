<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolePermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // роли
        $admin = Role::where('name', 'администратор')->first();
        $electronics = Role::where('name', 'электронщик')->first();
        $electrician = Role::where('name', 'электромеханик')->first();
        $foreman = Role::where('name', 'прораб')->first();
        $signalman = Role::where('name', 'связист')->first();
        $lifter = Role::where('name', 'лифтер')->first();
        $dispatcher = Role::where('name', 'диспетчер')->first();

        if ($admin->permissions()->first()) {
            return;
        }

        // права
        $editUsers = Permission::where('slug', 'edit-users')->first();
        $listUsers = Permission::where('slug', 'list-users')->first();
        $editRecords = Permission::where('slug', 'edit-records')->first();
        $listRecords = Permission::where('slug', 'list-records')->first();
        $editAddress = Permission::where('slug', 'edit-address')->first();
        $listAddress = Permission::where('slug', 'list-address')->first();
        $editDistrict = Permission::where('slug', 'edit-district')->first();
        $listDistrict = Permission::where('slug', 'list-district')->first();

        // права админа
        $permissionAdmin = Permission::where('id', '>', 0)->pluck('id')->toArray();
        $admin->permissions()->attach($permissionAdmin);

        // права электронщика
        $electronics->permissions()->attach($listUsers);
        $electronics->permissions()->attach($listRecords);
        $electronics->permissions()->attach($listAddress);
        $electronics->permissions()->attach($listDistrict);

        // права электромеханика
        $electrician->permissions()->attach($listUsers);
        $electrician->permissions()->attach($listRecords);
        $electrician->permissions()->attach($listAddress);
        $electrician->permissions()->attach($listDistrict);

        // права прораба
        $foreman->permissions()->attach($listUsers);
        $foreman->permissions()->attach($listRecords);
        $foreman->permissions()->attach($listAddress);
        $foreman->permissions()->attach($listDistrict);
        $foreman->permissions()->attach($editDistrict);

        // права связиста
        $signalman->permissions()->attach($listUsers);
        $signalman->permissions()->attach($listRecords);
        $signalman->permissions()->attach($listAddress);
        $signalman->permissions()->attach($listDistrict);

        // права лифтера
        $lifter->permissions()->attach($listUsers);
        $lifter->permissions()->attach($listRecords);
        $lifter->permissions()->attach($listAddress);
        $lifter->permissions()->attach($listDistrict);

        // права диспетчера
        $dispatcher->permissions()->attach($listUsers);
        $dispatcher->permissions()->attach($listRecords);
        $dispatcher->permissions()->attach($editRecords);
        $dispatcher->permissions()->attach($listAddress);
        $dispatcher->permissions()->attach($editAddress);
        $dispatcher->permissions()->attach($listDistrict);
    }
}
