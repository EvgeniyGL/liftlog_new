<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        if ($role::where('name', 'электронщик')->first()) {
            return;
        }
        $role->name = 'электронщик';
        $role->slug = 'electronics';
        $role->save();
        $role = new Role();
        $role->name = 'электромеханик';
        $role->slug = 'electrician';
        $role->save();
        $role = new Role();
        $role->name = 'прораб';
        $role->slug = 'foreman';
        $role->save();
        $role = new Role();
        $role->name = 'связист';
        $role->slug = 'signalman';
        $role->save();
        $role = new Role();
        $role->name = 'лифтер';
        $role->slug = 'lifter';
        $role->save();
        $role = new Role();
        $role->name = 'администратор';
        $role->slug = 'admin';
        $role->save();
        $role = new Role();
        $role->name = 'диспетчер';
        $role->slug = 'dispatcher';
        $role->save();
        $role = new Role();
        $role->name = 'супер админ';
        $role->slug = 'superadmin';
        $role->save();
    }
}






