<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();
        if ($user1::where('email', 'jhon@deo.com')->first()) {
            return;
        }
        $admin = Role::where('slug','admin')->first();
        $disp = Role::where('slug', 'dispatcher')->first();
        $editRecords = Permission::where('slug','edit-records')->first();
        $editUsers = Permission::where('slug','edit-users')->first();
        $user1->name = 'Глечиков Евгений Александрович';
        $user1->email = 'jhon@deo.com';
        $user1->phone = 12345678123;
        $user1->password = bcrypt('secret');
        $user1->save();
        $user1->roles()->attach($admin);
        $user1->permissions()->attach($editUsers);
        $user1->permissions()->attach($editRecords);
        $user2 = new User();
        $user2->name = 'Mike Thomas';
        $user2->email = 'mike@thomas.com';
        $user2->phone = 12345678123;
        $user2->password = bcrypt('secret');
        $user2->save();
        $user2->roles()->attach($disp);
        $user2->permissions()->attach($editRecords);
    }
}
