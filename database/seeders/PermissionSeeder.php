<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $editUser = new Permission();
        if ($editUser::where('slug', 'edit-users')->first()) {
            return;
        }
        $editUser->name = 'изменять пользователей';
        $editUser->slug = 'edit-users';
        $editUser->save();
        $editUser = new Permission();
        $editUser->name = 'просматривать пользователей';
        $editUser->slug = 'list-users';
        $editUser->save();
        $createTasks = new Permission();
        $createTasks->name = 'изменять записи';
        $createTasks->slug = 'edit-records';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'просматривать записи';
        $createTasks->slug = 'list-records';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'изменять объекты';
        $createTasks->slug = 'edit-address';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'просматривать объекты';
        $createTasks->slug = 'list-address';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'изменять участки';
        $createTasks->slug = 'edit-district';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'просматривать участки';
        $createTasks->slug = 'list-district';
        $createTasks->save();
    }
}
