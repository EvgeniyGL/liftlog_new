<?php

namespace App\Traits;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Eloquent\Collection;

/**
 * App\Traits\HasRolesAndPermissions
 *
 * @property Collection|Permission[] $permissions
 * @property Collection|Role[] $roles
 */
trait HasRolesAndPermissions
{
    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'users_permissions');
    }

    /**
     * @param mixed $roles
     *
     * @return bool
     */
    public function hasRole($roles)
    {
        if (is_string($roles)) {
            if ($this->roles->contains('slug', $roles)) {
                return true;
            }
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }

            return false;
        }

        return false;
    }

    /**
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        return (bool)$this->permissions->where('slug', $permission)->count();
    }

    /**
     * @param array $permission
     *
     * @return array
     */
    public function hasPermissionTo(array $permission): array
    {
        $allPermissions = $this->getAllPermissions($permission);
        $result = [];
        foreach ($allPermissions as $permissionClass) {
            $result[] = $this->hasPermissionThroughRole($permissionClass)
                || $this->hasPermission($permissionClass->slug);
        }

        return $result;
    }

    /**
     * @param Permission $permission
     *
     * @return bool
     */
    public function hasPermissionThroughRole(Permission $permission): bool
    {
        foreach ($permission->roles as $role) {
            if ($this->roles->contains($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $permissions
     *
     * @return Collection
     */
    public function getAllPermissions(array $permissions)
    {
        return Permission::with('roles')->whereIn('slug', $permissions)->get();
    }

    /**
     * @param mixed ...$permissions
     *
     * @return $this
     */
    public function givePermissionsTo(...$permissions): self
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->saveMany($permissions);

        return $this;
    }

    /**
     * @param mixed ...$permissions
     *
     * @return $this
     */
    public function deletePermissions(...$permissions): self
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);

        return $this;
    }

    /**
     * @param mixed ...$permissions
     *
     * @return $this
     */
    public function refreshPermissions(...$permissions): self
    {
        $this->permissions()->detach();

        return $this->givePermissionsTo($permissions);
    }

}
