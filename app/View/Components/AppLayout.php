<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Support\Facades\View as FasadeView;

class AppLayout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return Factory|View|FasadeView
     */
    public function render()
    {
        return view('layouts.app');
    }
}
