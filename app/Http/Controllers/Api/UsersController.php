<?php

namespace App\Http\Controllers\Api;

use App\Models\Role;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Helpers\View\SharedDataJS;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class UsersController extends ApiController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    public function add()
    {
        $users = User::with('roles', 'permissionsByRole')->get();

        return compact('users');
    }
}
