<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\User\GetAllUsersRequest;
use App\Tasks\Role\GetAllRolesTask;
use App\Tasks\User\GetAllUsersTask;
use Illuminate\Contracts\View\Factory;
use App\Helpers\View\SharedDataJS;
use Illuminate\Support\Facades\View as FasadeView;
use Illuminate\View\View;

class UsersController extends WebController
{
    /**
     * @var SharedDataJS
     */
    private $sharedDataJS;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sharedDataJS = new SharedDataJS();
    }

    /**
     * @param GetAllUsersRequest $request
     *
     * @return Factory|View|FasadeView
     */
    public function index(GetAllUsersRequest $request)
    {
        $users = app(GetAllUsersTask::class)->roles()->ordered()->run();
        $roles = app(GetAllRolesTask::class)->withoutAdmin()->run();

        $data = compact('users', 'roles');
        $this->sharedDataJS->put($data);
        return view('users.index');
    }
}
