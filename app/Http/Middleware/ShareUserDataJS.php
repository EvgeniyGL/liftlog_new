<?php

namespace App\Http\Middleware;

use App\Helpers\View\SharedDataJS;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShareUserDataJS
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $sharedDataJS = new SharedDataJS();

        /** @var User|null $user */
        $user = Auth::user();

        if ($user) {
            $sharedDataJS->put(
                [
                    'id' => $user->id,
                    'name' => $user->name,
                    'roles' => $user->roles,
                ],
                "currentUser"
            );
        }

        return $next($request);
    }
}
