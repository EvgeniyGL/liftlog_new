<?php

namespace App\Http\Middleware;

use App\Exceptions\Api\JsonRpcException;
use App\Http\Controllers\Api\ApiController;
use App\Http\Responses\JsonRpcResponse;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JsonRpcServer
{
    public function handle(Request $request, ApiController $controller)
    {
        try {
            $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

            if (empty($content)) {
                throw new JsonRpcException('Parse error', JsonRpcException::PARSE_ERROR);
            }
            $result = $controller->{$content['method']}(...[$content['params']]);

            return JsonRpcResponse::success($result, $content['id']);
        } catch (\Exception $e) {
            return JsonRpcResponse::error($e->getMessage(), $e->getCode());
        }
    }
}
