<?php

namespace App\Http\Responses;

class JsonRpcResponse
{
    const JSON_RPC_VERSION = '2.0';

    public static function success($result, string $id = null): array
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'result'  => $result,
            'id'      => $id,
        ];
    }

    public static function error(string $message = 'UNKNOWN_ERROR', int $code = 0): array
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error'   => ['code' => $code, 'message' => $message],
            'id'      => null,
        ];
    }
}
