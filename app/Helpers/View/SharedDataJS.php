<?php

namespace App\Helpers\View;

use Coderello\SharedData\Facades\SharedData;

/**
 * Helper for put data in global window browser for javascript
 * Class SharedDataJS
 * @package App\Helpers
 */
class SharedDataJS
{
    /**
     * Помещает данные во view window.SharedData.
     *
     * @param  array        $data  массив данных для передачи.
     * @param  null|string  $name  ключ в массиве или массив данных
     *
     * @return void
     */
    public function put(array $data, $name = null): void
    {
        if(is_iterable($data) && !$name){
            SharedData::put($data);
        }else {
            SharedData::put([$name => $data]);
        }
    }
}
