<?php

namespace App\Tasks\Role;

use App\Criterias\User\AdminsCriteria;
use App\Criterias\User\RoleCriteria;
use App\Criterias\User\UserWithRoleCriteria;
use App\Criterias\Role\WithoutAdminCriteria;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Base\Criterias\OrderByCreationDateDescendingCriteria;
use App\Base\Parents\Tasks\Task;

class GetAllRolesTask extends Task
{
    protected RoleRepository $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->all();
    }

    public function withoutAdmin(): self
    {
        $this->repository->pushCriteria(new WithoutAdminCriteria());
        return $this;
    }

}
