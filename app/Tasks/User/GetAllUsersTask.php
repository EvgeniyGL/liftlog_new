<?php

namespace App\Tasks\User;

use App\Criterias\User\AdminsCriteria;
use App\Criterias\User\RoleCriteria;
use App\Criterias\User\UserWithRoleCriteria;
use App\Repositories\UserRepository;
use App\Base\Criterias\OrderByCreationDateDescendingCriteria;
use App\Base\Parents\Tasks\Task;

class GetAllUsersTask extends Task
{
    protected UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->all();
    }

    public function admins(): self
    {
        $this->repository->pushCriteria(new AdminsCriteria());
        return $this;
    }

    public function ordered(): self
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this;
    }

    public function withRole($roles): self
    {
        $this->repository->pushCriteria(new RoleCriteria($roles));
        return $this;
    }

    public function roles(): self
    {
        $this->repository->pushCriteria(new UserWithRoleCriteria());
        return $this;
    }

}
