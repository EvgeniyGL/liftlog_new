<?php

namespace App\Criterias\Role;

use App\Base\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class WithoutAdminCriteria extends Criteria
{
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereNotIn('slug', ['admin', 'superadmin']);
    }
}
