<?php

namespace App\Criterias\User;

use App\Base\Parents\Criterias\Criteria;
use App\Models\Role;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class UserWithRoleCriteria extends Criteria
{
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->with('roles');
    }
}
