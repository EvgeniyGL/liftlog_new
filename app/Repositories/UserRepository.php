<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Models\User;

class UserRepository extends BaseRepository
{

    /**
     * Specify Model class name
     */
    public function model(): string
    {
        return User::class;
    }
}
