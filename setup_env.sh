#!/bin/bash
echo DB_DATABASE=${DB_DATABASE} >> .env.example
echo DB_USERNAME=${DB_USERNAME} >> .env.example
echo DB_PASSWORD=${DB_PASSWORD} >> .env.example
echo POSTGRES_DB=${POSTGRES_DB} >> .env.example
echo POSTGRES_USER=${POSTGRES_USER} >> .env.example
echo POSTGRES_PASSWORD=${POSTGRES_PASSWORD} >> .env.example
echo APP_KEY=${APP_KEY} >> .env.example
