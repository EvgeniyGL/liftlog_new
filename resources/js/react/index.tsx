import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import './users';
import {Alert} from "@core/services/alert/components";

interface ISharedData {
    [key: string]: any
}

declare global {
    interface Window {
        /**
         * Data sanded from backend in global variable window for javascript
         */
        sharedData: ISharedData;
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(
            <React.Fragment>
                <Alert/>
            </React.Fragment>,
        document.getElementById('root')
    );
}


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

//example multiple apps
/*
function getUrlVars() {
	var vars = [], hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) {
	  hash = hashes[i].split('=');
	  vars.push(hash[0]);
	  vars[hash[0]] = hash[1];
	}
	return vars;
  }

  var urlParams = getUrlVars();

  switch (urlParams["startPage"]) {
	case "SecondApp":
	  ReactDOM.render(<SecondApp />, document.getElementById('root'));
	  break;

	case undefined:
	default:
	  ReactDOM.render(<App />, document.getElementById('root'));
	  break;
  }*/
