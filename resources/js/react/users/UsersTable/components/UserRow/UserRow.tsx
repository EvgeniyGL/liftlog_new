import * as React from "react";
import {IUser} from "@react/Root/store";
import {observer} from "mobx-react";
import {RolesList} from "@react/users/UsersTable/components/RolesList/RolesList";

interface IProps {
    selectUser: (id: number) => void
    user: IUser
    selectedUser: IUser
}

export const UserRow = observer(({user, selectedUser, selectUser}: IProps) => (
        <tr data-id={user.id}
            key={user.id}
            className={selectedUser?.id === user.id ? 'table-active' : ''}
            onClick={(e) => selectUser(+e.currentTarget.dataset.id)}
        >
            <td className='td-login'>
                {user.email}
            </td>
            <td className='td-name'>
                {user.name}
            </td>
            <td className='td-role'>
                <RolesList roles={user.roles}/>
            </td>
            <td className='td-phone'>
                {user.phone}
            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    )
)
