import * as React from "react";

export const PermissionsList = function (props) {

    const list = props.permissions.map((permission) => {
        return <span key={permission.id} data-id={permission.id}>{permission.name}</span>
    })

    return <div>{list}</div>
}
