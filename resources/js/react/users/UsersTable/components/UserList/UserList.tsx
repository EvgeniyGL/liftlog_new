import * as React from "react";
import {IUser} from "@react/Root/store";
import {observer} from "mobx-react";
import {UserRow} from "@react/users/UsersTable/components/UserRow/UserRow";

interface IProps {
    selectUser: (id: number) => void
    users: IUser[]
    selectedUser: IUser
}

export const UserList = observer((props: IProps) => (
    <>
        {props.users.map(user => (
            <UserRow selectedUser={props.selectedUser}
                     user={user}
                     key={user.id}
                     selectUser={props.selectUser}
            />
        ))}
    </>
))
