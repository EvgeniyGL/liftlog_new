import * as React from "react";
import {Key} from "@core/Icons/Key";
import * as S from './RolesListStyle'
import {IRole, IPermission} from "@react/Root/store";
import {ToolTip} from "@core/components/tooltips/ToolTip";

interface IProps {
    roles: IRole[];
}

export const RolesList = function (props: IProps) {

    const permissions = props.roles.map((role) => {
        return role.permissions.map((permission: IPermission) => {
            return permission.name
        }).join(', ')
    }).join()

    const roles = props.roles.map((role) => {
        return <React.Fragment key={role.id}>
            <span data-id={role.id}>{role.name}</span>
            <ToolTip toolTipText={permissions}>
                <Key/>
            </ToolTip>
        </React.Fragment>
    })

    return <S.Container>{roles}</S.Container>
}
