import {action, computed, observable} from "mobx";
import {ICurrentUser, IOption, IRole, IUser} from "@react/Root/store";
import {alerts} from "@core/services/alert";
import {API} from "@core/services/api/api";
import {IFormUser} from '@react/users/UserForm/UserForm';

export class UsersTableStore {

    @observable public users: IUser[] = window.sharedData.users || [];
    @observable public roles: IUser[] = window.sharedData.roles || [];
    @observable public selectedUser: IUser | null = null;

    public userFormInitialValues = {
        id: '',
        email: '',
        name: '',
        roleId: '',
        phone: '',
    };

    @action selectUser = (id: number) => {

        this.selectedUser = this.users.filter(user => user.id === id)[0];
        console.log('log--name', '|\n',
            '--id=', id, '|\n',
            '--this.selectedUser=', this.selectedUser, '|\n',
            '--this.users=', this.users, '|\n',
        );
    };

    @computed get userFormValues(): IFormUser {

        const values = this.userFormInitialValues;
        if (!this.selectedUser) {
            return values;
        }
        Object.keys(values).map(key => {
            if (this.selectedUser[key]) {
                values[key] = this.selectedUser[key];
            }
        });
        console.log('log--name', '|\n',
            '--values=', values, '|\n',
        );
        return values;
    };

    @computed get roleOptions(): IOption[] {

        console.log('log--name', '|\n',
            '--this.roles=', this.roles, '|\n',
        );
        return this.roles.map(role => ({
            value: `${role.id}`,
            label: role.name,
        }));
    };

}
