import * as React from "react";
import {UsersTableStore} from './UsersTableStore'
import {inject, observer} from "mobx-react";
import {UserList} from "@react/users/UsersTable/components/UserList/UserList";

interface IProps {
    UsersTableStore: UsersTableStore
}

@inject('UsersTableStore')
@observer
export class UsersTable extends React.Component<IProps> {
// todo remove check

    render() {
        const {selectUser, selectedUser, users} = this.props.UsersTableStore

        return <table id="table_users" className="table table-bordered table-hover paginated">
            <thead>
            <tr className="bg-info text-light">
                <th>Эл. почта</th>
                <th>ФИО</th>
                <th>Должность</th>
                <th>Телефон</th>
                <th>Оповещение</th>
                <th>Закрепленные объекты</th>
            </tr>
            </thead>
            <tbody>
            <UserList selectUser={selectUser} selectedUser={selectedUser} users={users}/>
            </tbody>
        </table>
    }
}
