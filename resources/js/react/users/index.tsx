import * as React from 'react';
import {ModalProvider} from 'styled-react-modal';
import ReactDOM from "react-dom";
import {Provider} from "mobx-react";
import {stores} from "./stores";
import {UsersTable} from "@react/users/UsersTable/UsersTable";
import {UsersTableStore} from "@react/users/UsersTable/UsersTableStore";
import {ButtonPanel} from "@react/users/ButtonPanel/ButtonPanel";
import {UserForm} from '@react/users/UserForm/UserForm';
import {UsersFormStore} from '@react/users/UserForm/UsersFormStore';

class Users extends React.Component {
    public render(): React.ReactNode {
        return (
            <React.Fragment>
                <ModalProvider>
                    {/* @ts-ignore */}
                    <UsersTable/>
                    {/* @ts-ignore */}
                    <ButtonPanel />
                    {/* @ts-ignore */}
                    <UserForm />
                </ModalProvider>
            </React.Fragment>
        );
    }
}


if (document.getElementById('users-list')) {
    ReactDOM.render(
        <Provider {...stores}>
            <Users/>
        </Provider>,
        document.getElementById('users-list'),
    );
}
