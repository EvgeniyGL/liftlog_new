import * as React from "react";
import {inject, observer} from "mobx-react";
import ButtonBar from "@core/components/buttons/ButtonBar";
import {UsersTableStore} from './../UsersTable/UsersTableStore';
import {UsersFormStore} from './../UserForm/UsersFormStore';
import {alerts} from '@core/services/alert';

interface IProps {
    UsersFormStore: UsersFormStore,
    UsersTableStore: UsersTableStore
}

@inject('UsersFormStore', 'UsersTableStore')
@observer
export class ButtonPanel extends React.Component<IProps> {

    selectedUser;

    toggleModal = this.props.UsersFormStore.toggleModal;
    setType = this.props.UsersFormStore.setType;

    add = () => {
        this.setType('add');
        this.toggleModal();
    };
    edit = () => {
        if (!this.selectedUser) {
            alerts.info('Выберите пользователя!');
            return;
        }
        this.setType('edit');
        this.toggleModal();
    };
    remove = () => {
        if (!this.selectedUser) {
            alerts.info('Выберите пользователя!');
            return;
        }
        this.setType('remove');
        this.toggleModal();
    };

    render() {

        this.selectedUser = this.props.UsersTableStore.selectedUser;

        return (
            <>
                <ButtonBar
                    add={this.add}
                    edit={this.edit}
                    remove={this.remove}
                />
            </>
        );
    }
}
