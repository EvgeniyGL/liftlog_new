import * as Yup from 'yup';

import {validateMessages} from '@core/constants/validateMessages';

export const scheme = () => {
    return Yup.object().shape({
        email: Yup.string().email(validateMessages.EMAIL).required(validateMessages.REQUIRED),
        name: Yup.string().required(validateMessages.REQUIRED),

    })
};
