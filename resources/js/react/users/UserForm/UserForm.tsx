import * as React from "react";
import {inject, observer} from "mobx-react";
import ModalWrapper from "@core/components/modal";
import {UsersTableStore} from './../UsersTable/UsersTableStore';
import {TypeForm, UsersFormStore} from './UsersFormStore';
import {Formik, FormikProps, validateYupSchema, yupToFormErrors} from 'formik';
import {scheme} from './scheme';
import Button from '@core/forms/button';
import * as S from './UserFormStyle';
import {IRole, IUser} from '@react/Root/store';
import Input from '@core/forms/input';
import {observable} from 'mobx';
import Select from '@core/forms/select';

interface IProps {
    UsersTableStore: UsersTableStore,
    UsersFormStore: UsersFormStore
}

export interface IFormUser {
    id: string;
    email: string;
    name: string;
    roleId: string;
    phone: string;
}

@inject('UsersFormStore', 'UsersTableStore')
@observer
export class UserForm extends React.Component<IProps> {

    validate = (values: IFormUser) => {
        console.log('log--validate', '|\n',
            '--values=', values, '|\n',
        );
        const validateScheme = scheme();
        try {
            validateYupSchema<IFormUser>(values, validateScheme, true);
        } catch (error) {
            return yupToFormErrors(error);
        }
    };

    onSubmit = async (values) => {
        console.log('log--onSubmit', '|\n',
            '--values=', values, '|\n',
        );
    };

    render() {
        const {userFormValues, userFormInitialValues, roleOptions} = this.props.UsersTableStore;
        const {isOpen, title, load, toggleModal, type} = this.props.UsersFormStore;

        const initialValues: IFormUser = type === 'add' ? userFormInitialValues : userFormValues;

        return (
            <ModalWrapper isOpen={isOpen} toggleModal={toggleModal} title={title}>
                <Formik
                    validateOnBlur
                    enableReinitialize
                    onSubmit={values => this.onSubmit(values)}
                    validate={values => this.validate(values)}
                    validateOnChange={false}
                    initialValues={initialValues}
                    render={({values, errors, setFieldValue, handleSubmit}: FormikProps<IFormUser>) => (
                        <form onSubmit={handleSubmit} className="modal-body">
                            <div className="form-group">
                                <input type="hidden" name="id" value={values.id}/>
                                <Input type="text"
                                       name="email"
                                       value={values.email}
                                       placeholder="Эл. почта"
                                       onChange={e => setFieldValue('email', e.target.value)}
                                       error={errors.email}
                                />
                                <Input
                                    type="text"
                                    name="theme"
                                    error={errors.name}
                                    value={values.name}
                                    placeholder="ФИО"
                                    onChange={e => setFieldValue('name', e.target.value)}
                                />
                                <Select
                                    name="maker_id"
                                    onChange={e => setFieldValue('maker_id', e.target.value)}
                                    options={roleOptions}
                                    value={values.roleId}
                                    error={errors.roleId}
                                    initialValue={"Выберите должность"}
                                    required
                                />
                            </div>
                            <Button type="submit" disabled={load}>
                                Сохранить
                            </Button>
                        </form>
                    )}
                />
            </ModalWrapper>
        );
    }
}

