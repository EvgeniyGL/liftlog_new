import {action, computed, observable} from "mobx";
import {IUser} from "@react/Root/store";
import {alerts} from "@core/services/alert";
import {API} from "@core/services/api/api";
import {IFormUser} from '@react/users/UserForm/UserForm';

export type TypeForm = 'add' | 'edit' | 'remove';

export enum Title {
    add = 'Добавить пользователя',
    edit = 'Редактировать пользователя',
    remove = 'Удалить пользователя'
}

export class UsersFormStore {

    private uri = 'users';

    @observable public isOpen = false;
    @observable public type: TypeForm = 'add';
    @observable public load: boolean = false;

    @action toggleModal = (isOpen?: boolean) => {
        this.isOpen = isOpen === undefined ? !this.isOpen : isOpen;
    };

    @action setType = (type: TypeForm) => {
        this.type = type;
    };

    @computed get title(): string {
        return Title[this.type];
    };

    @action add = async () => {
        const result = await API.setHandleError(false).post<{ ok: boolean }>(this.uri, 'add', '');
        result.ok ? alerts.success('Пользователь добавлен!') : alerts.warning('Не удалось добавить пользователя!');
    };

    @action edit = () => {
    };

    @action remove = () => {
    };

}
