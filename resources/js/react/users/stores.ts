import {UsersTableStore} from "@react/users/UsersTable/UsersTableStore";
import {UsersFormStore} from '@react/users/UserForm/UsersFormStore';

interface IStores {
    [name: string]: any;
}

export const stores: IStores =
    (document.getElementById('users-list')) ? {
        UsersTableStore: new UsersTableStore(),
        UsersFormStore: new UsersFormStore(),
    } : null;
