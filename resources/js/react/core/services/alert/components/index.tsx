import React from 'react';
import ReactDOM from 'react-dom';
import * as S from './style';
import {Subject, Subscription} from 'rxjs';
import {IAlert} from "@core/services/alert";

interface IState {
    bottom: string;
    message: string;
    type: string;
}

export const alerts$ = new Subject<IAlert>();

const root = document.getElementById("root");

export class Alert extends React.Component<{}, IState> {

    private alertsStream: Subscription = new Subscription();

    state = {
        bottom: '-82',
        message: '',
        type: 'none',
    };

    private showAlert = (alert: IAlert) => {
        this.setState({
            bottom: '0',
            message: alert.message,
            type: alert.type,
        });
        setTimeout(this.hideAlert, 5000);
    };

    public componentDidMount(): void {
        this.alertsStream = alerts$.subscribe(this.showAlert);
    }

    private hideAlert = () => {
        this.setState({
            bottom: '-82',
        });
    };

    render(): React.ReactElement {
        const {bottom, type, message} = this.state;
        return ReactDOM.createPortal(
            <S.Container bottom={bottom} type={type}>{message}</S.Container>,
            root,
        );
    }
}
