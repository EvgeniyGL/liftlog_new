/**
 * The options used to configure the API.
 */
import {baseUrl} from "../../../../config";


export interface ApiConfig {
  /**
   * The URL of the api.
   */
  url: string

  /**
   * Milliseconds before we timeout the request.
   */
  timeout: number
}

/**
 * The default configuration for the app.
 */
export const DEFAULT_API_CONFIG: ApiConfig = {
  url: baseUrl,
  timeout: 10000,
}
