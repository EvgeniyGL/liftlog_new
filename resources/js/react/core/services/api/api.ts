import axios, {AxiosInstance} from 'axios';
import {ApiConfig, DEFAULT_API_CONFIG} from "@core/services/api/api-config";
import {alerts} from "@core/services/alert";

export type ApiSuccessResponse<T> = {
    jsonrpc: "2.0";
    result?: T;
    id: number;
    error?: false;
}

type ApiError = { code: number, message: string }

export type ApiErrorResponse<T> = {
    jsonrpc: "2.0";
    error: ApiError;
    id: null;
    result?: null;
}

export interface ApiRequestData {
    jsonrpc: "2.0";
    method: string;
    params: any;
    id: number;
}

type ApiRequest = (url: string, payload?: Record<string, any>) => Promise<ApiResponse<any>>;

interface ApiRequestParams {
    uri: string,
    data?: Record<string, any>
}

export type ApiResponse<T, U = T> = { data: ApiErrorResponse<U> } | { data: ApiSuccessResponse<T> };

class Api {
    /**
     * The underlying api instance which performs the requests.
     */
    api: AxiosInstance

    /**
     * Configurable options.
     */
    config: ApiConfig

    /**
     * Configurable options.
     */
    handleError: boolean = true

    /**
     * Creates the api.
     *
     * @param config The configuration to use.
     */
    constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
        this.config = config
    }

    /**
     * Sets up the API.  This will be called during the bootup
     * sequence and will happen before the first React component
     * is mounted.
     *
     * Be as quick as possible in here.
     */
    setup = async () => {
        const options = {
            baseURL: this.config.url,
            timeout: this.config.timeout,
            headers: {
                Accept: "application/json",
            },
        }
        this.api = axios.create(options)
    }

    post = async <T>(uri: string, method: string, params: any, id?: number) => {
        await this.setup()
        const requestData: ApiRequestData = {
            id: id || new Date().getTime(),
            method: method,
            params: params,
            jsonrpc: "2.0"
        }
        try {
            const response: ApiResponse<T> = await this._handleResponse(this.api.post, {
                uri: uri,
                data: requestData
            })
            const data = response.data
            if (data.error) {
                return {ok: false}
            }
            return {ok: true, ...data.result}
        } catch (error) {
            this._showError(error)
            return {ok: false}
        }
    }

    _handleResponse = async <T, U>(apiRequest: ApiRequest, params: ApiRequestParams): Promise<ApiResponse<T | U>> => {
        const res = await apiRequest(params.uri, params.data)
        return this._handleError(res)
    }

    _handleError = <T, U>(res: ApiResponse<T | U>) => {
        if (res.data.error && this.handleError) {
            this._showError(res.data.error)
        }

        return res
    }

    _showError = (error?: ApiError) => {
        if (error.message) {
            const msg = `Ошибка сети: ${error?.message || 'Неизвестно'}!`;
            alerts.danger(msg)
        }
        const log = `Ошибка сети. Url: ${this.config.url}. Код: ${error?.code || 'неизвестен'}!`;
        console.error(log, error)
    }

    public setHandleError(handle: boolean) {
        this.handleError = handle;
        return this;
    }
}


export const API = new Api();
