import styled, { css } from 'styled-components';
import { IProps } from './index';

export const ComponentCheckbox = styled.input.attrs({
	className: 'form-check-input',
})`
    left: 20px;
`;

export const Wrapper = styled.div.attrs({
	className: 'form-check input-group ',
})``;
export const Label = styled.label.attrs<{for:string}>({
	className: 'form-check-label',
})``;
