import * as React from 'react';
import * as S from './styles';

export interface IProps {
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	id?: string;
	value: string;
	name: string;
	checked: boolean;
	label?: string;
	type: 'checkbox';
}

const Checkbox: React.FunctionComponent<IProps> = props => {
	return (
		<S.Wrapper>
			<S.Label>{props.label||''}
			<S.ComponentCheckbox {...props} /></S.Label>
		</S.Wrapper>
	);
};
export default Checkbox;
