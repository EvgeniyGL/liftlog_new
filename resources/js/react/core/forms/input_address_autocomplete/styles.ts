import styled from 'styled-components';

export const Label = styled.label`
	font-size: 12px;
`;

export const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
	font-size: 12px; 
    padding: 10px 0;
`;
