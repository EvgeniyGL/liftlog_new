import React from 'react';
import SelectAsync from "@core/forms/async_select";
import {IAddresses} from "@react/Root/store";
import {IOption} from "@react/Root/store";
import * as S from "./styles";

interface IProps {
    selectAddress: (id: string, label: string) => void
    placeholder?: string
    label?: string
    initialText: string
    changeFieldText: (text: string) => void
}

interface IState {
    optionsAdr: IOption[];
}

export class InputAddressAutocomplete extends React.Component<IProps, IState> {

    state = {
        optionsAdr: [{value: '', label: ''}]
    }

    // //event search address
    // subjectForDebounceLoadAddress$ = new Subject<string>()
    // subjectForResultAdr$: Subscription = new Subscription

    freeze = false
    timer
    loadAddress = async (strSearch: string) => {
        this.freeze = true
        return new Promise(async (res) => {
            let p = new Promise((res) => {
                if (this.freeze) clearTimeout(this.timer)
                this.timer = setTimeout(async () => {
                    this.freeze = false
                    const r = await this.load(strSearch)
                    res(r);
                }, 300)
            })

            p.then(function (x) {
                res(x);
            })
        });
    };

    load = async (strSearch: string) => {
        // const response = await http.post<IAddresses>('address/loadAddress', JSON.stringify({str_search: strSearch}));
        // return Object.keys(response.data).map((key: string) => {
        //     return {value: response.data[key].address_id, label: response.data[key].address_name};
        // });
    }

    // //emit event search address
    // emitLoad = async (strSearch: string) => {
    // 	this.subjectForDebounceLoadAddress$.next(strSearch)
    // }
    //
    // componentDidMount() {
    // 	//subscribe on event search address
    // 	this.subjectForResultAdr$ = this.subjectForDebounceLoadAddress$.pipe(debounceTime(500)).subscribe((strSearch) => {
    // 		return this.loadAddress(strSearch)
    // 	})
    // }
    //
    // componentWillUnmount() {
    // 	this.subjectForResultAdr$.unsubscribe()
    // }

    selectAddress = (id: string, label: string) => {
        this.props.selectAddress(id, label);
    };

    changeInputAddress = (text: string) => {
        this.props.changeFieldText(text);
    };

    render() {
        return <S.Wrapper>
            {this.props.label && <S.Label>{this.props.label}</S.Label>}
            <SelectAsync changeInput={this.changeInputAddress}
                         inputText={this.props.initialText}
                         load={this.loadAddress}
                         options={this.state.optionsAdr}
                         pickValue={this.selectAddress}
                         placeholder={this.props.placeholder}
            />
        </S.Wrapper>;
    }
}
