import * as React from 'react';
import { Component } from './styles';

export type ButtonSize = 'sm' | 'md' | 'lg';

export interface IProps {
	size?: ButtonSize;
	disabled?: boolean;
	active?: boolean;
	onClick?: () => void;
	id?: string;
	margin?: string;
	bgcolor?: string;
	color?: string;
	type: 'button' | 'submit' | 'reset' | undefined;
}

const Button: React.FunctionComponent<IProps> = props => {
	return <Component {...props}>{props.children}</Component>;
};
export default Button;
