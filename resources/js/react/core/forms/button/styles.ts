import styled, { css } from 'styled-components';
import { IProps } from './index';

// const getButtonStyles = (props: IProps) => {
// 	return css`
// 		background-color: ${props.bgcolor !== undefined ? props.bgcolor : '#fff'};
// 		color: ${props.color !== undefined ? props.color : '#8b8b8b'};

// 		${getButtonSizeStyles};

// 		${props.disabled &&
// 			`cursor: not-allowed;
// 		pointer-events: none;
// 		background-color: '#ccc';
// 		border-color: '#ccc';
// 		color: '#fff';
// 	`};

// 		${props.active &&
// 			`background-color: '#007bff';
// 			border-color: '#007bff';
// 			cursor:pointer
// 			color: '#fff';
// 	`};

// 		margin: ${props.margin !== undefined ? props.margin : '10'}px 0;
// 	`;
// };

export const Component = styled.button.attrs<{ disabled: boolean }>({
	className: 'btn-sm btn-outline-primary',
})``;
// ${getButtonStyles}
