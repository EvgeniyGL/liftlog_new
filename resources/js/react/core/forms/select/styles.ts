import styled from 'styled-components';

export const ComponentSelect = styled.select.attrs({
    className: 'form-control',
})`
`;

export const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
	font-size: 12px; 
    padding: 10px 0;
`;

export const Label = styled.label`
	font-size: 12px;
`;

export const Error = styled.div.attrs({
    className: 'text-danger',
})`
	font-size: 12px;
	width: 100%;
`;
