import * as React from 'react';
import * as S from './styles';
import { optionCSS } from 'react-select/src/components/Option';
import { ShadowPropTypesIOS } from 'react-native';
import { IOption } from 'resources/js/react/Root/store';

export interface IProps {
	// placeholder?: string;
	onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
	id?: string;
	value: string;
	name: string;
	options: IOption[];
	label?: string;
	error?: string;
	initialValue:string;
	required?: boolean
}

const Select: React.FunctionComponent<IProps> = props => {
	return (
		<S.Wrapper>
			{props.label && <S.Label>{props.label}</S.Label>}
			<S.ComponentSelect name={props.name} value={props.value} onChange={props.onChange}>
				<option value={''} disabled={props.required}>
					{props.initialValue}
				</option>
				{props.options.map(option => (
					<option key={option.value} value={option.value}>
						{option.label}
					</option>
				))}
			</S.ComponentSelect>
			<S.Error>{props.error}</S.Error>
		</S.Wrapper>
	);
};
export default Select;
