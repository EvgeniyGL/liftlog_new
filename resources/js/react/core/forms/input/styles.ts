import styled, { css } from 'styled-components';
import { IProps } from './index';

const baseStyles = css`
	width: 50%;
	padding: 4px;
	font-size: 16px;
	font-family: Roboto,sans-serif;
`;


export const ComponentInput = styled.input<{ error?: string }>`
  color: ${props => props.error ?
			`red;`:''
	};
`;

export const ErrorInput = styled.div.attrs({
	className: 'text-danger',
})`
	font-size: 12px;
	width: 100%;
`;
export const Wrapper = styled.div.attrs({
	className: 'input-group input-group-sm mb-3',
})`
	font-size: 12px;
`;
export const Label = styled.label`
	font-size: 12px;
`;
