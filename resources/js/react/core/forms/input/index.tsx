import * as React from 'react';
import * as S from './styles';

export interface IProps {
	placeholder?: string;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
	id?: string;
	value: string;
	type: 'text';
	error?: string;
	name: string;
	checked?: boolean;
	label?: string;
}

const Input: React.FunctionComponent<IProps> = props => {
	return (
		<S.Wrapper>
			{props.label && <S.Label>{props.label}</S.Label>}
			<S.ComponentInput className="form-control" {...props} />
			<S.ErrorInput>{props.error}</S.ErrorInput>
		</S.Wrapper>
	);
};
export default Input;
