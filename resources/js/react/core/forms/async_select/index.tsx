import React from 'react';
import AsyncSelect from 'react-select/async';
import {IOption} from '@react/Root/store';

interface IProps {
    options: IOption[];
    changeInput: (val: string) => void;
    pickValue: (id: string, label: string) => void;
    inputText: any; //TODO check type not working
    // inputValue: string;
    load: (val: string, callback: (options: ReadonlyArray<IOption>) => void) => void;
    placeholder?: string;
}

const SelectAsync: React.FC<IProps> = props => {

    const onInputChange = (newValue: string, action: { action: string }) => {
        switch (action.action) {
            case 'input-change':
                props.changeInput(newValue);
                break;
            default:
                return;
        }
    };

    const onChangeFunc = (optionSelected: any, action: any) => {
        switch (action.action) {
            case 'select-option':
                props.pickValue(optionSelected.value, optionSelected.label);
                return;
            default:
                return;
        }
    };

    return (
        <AsyncSelect
            inputValue={props.inputText}
            value={props.inputText}
            closeMenuOnSelect={false}
            // components={makeAnimated()}
            placeholder={props.placeholder || "Поиск адреса..."}
            loadOptions={props.load}
            onChange={onChangeFunc}
            defaultOptions={props.options}
            onInputChange={onInputChange}
            noOptionsMessage={() => 'Ничего не найдено'}
        />
    );
};

export default SelectAsync;
