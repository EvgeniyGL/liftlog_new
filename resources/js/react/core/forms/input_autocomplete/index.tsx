import * as React from 'react';
import Autocomplete from 'react-autocomplete';
import * as S from './style';

interface IProps {
    items: string[];
    placeholder: string;
    setFieldValue: (field: string, val: string) => void;
    value: string;
    name: string;
    error?: string;
}

class InputAutocomplete extends React.Component<IProps> {
    filterItems = (items: string[]): string[] => {
        const {value} = this.props;
        return !!value ? items.filter(item => item.includes(value)) : items;
    };

    render() {
        const {setFieldValue, name, value, placeholder, items} = this.props;

        return (
            <S.Wrapper>
                <Autocomplete
                    getItemValue={item => item}
                    items={this.filterItems(items)}
                    renderItem={(item, isHighlighted) => (
                        <S.Item key={item}>
                            {item}
                        </S.Item>
                    )}
                    menuStyle={{
                        background: '#f6f6f6',
                        position: 'fixed',
                        overflow: 'auto',
                        maxHeight: '30%', // TODO: don't cheat, let it flow to the bottom
                    }}
                    value={value}
                    onChange={e => setFieldValue(name, e.target.value.toUpperCase())}
                    onSelect={value => setFieldValue(name, value.toUpperCase())}
                    inputProps={{placeholder: placeholder}}
                />
                <S.Error>{this.props.error}</S.Error>
            </S.Wrapper>
        );
    }
}

export default InputAutocomplete;
