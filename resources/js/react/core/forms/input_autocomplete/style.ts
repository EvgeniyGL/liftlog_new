import styled from 'styled-components';

export const Wrapper = styled.div`
    font-size: 12px;
	& div {
		z-index: 10000;
        opacity: 1;    
        min-width: 250px;
	}
	& input {
        width: 100%;
	}
`;

export const Error = styled.div.attrs({
    className: 'text-danger',
})`
	font-size: 12px;
	width: 100%;
`;

export const Item = styled.div`
	font-size: 14px;
	border-bottom: #1b4b72 1px solid;
	cursor:pointer;
	padding: 0 5px;
	opacity: 1;
	&:hover{
	    background: #1b4b72;
	    color: #fff;
	}
`;
