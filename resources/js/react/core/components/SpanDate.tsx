import React from 'react';

interface IProps {
  dateString: string;
}

const getDate = (dateStr: string) => {
  const date = new Date(dateStr);
  const strDate =
    ('0' + date.getDate()).slice(-2) +
    '.' +
    ('0' + (date.getMonth() + 1)).slice(-2) +
    '.' +
    date.getFullYear();
  return strDate;
};

const SpanDate: React.FC<IProps> = props =>
  !props.dateString ? null : <span>{getDate(props.dateString)}</span>;
export default SpanDate;
