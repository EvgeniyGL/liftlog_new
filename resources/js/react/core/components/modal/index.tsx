import React, {ReactChild} from 'react';
import Modal from 'react-modal';
import * as S from './style';
import {BtnCloseModal} from '@core/components/buttons/BtnCloseModal/BtnCloseModal';

interface IProps {
    children: ReactChild;
    isOpen: boolean;
    toggleModal: (isOpen: boolean) => void;
    transparent?: boolean
    withOutCross?: boolean
    stickBottom?: boolean
    title: string
}

Modal.setAppElement('body');

class ModalWrapper extends React.Component<IProps> {

    close = () => this.props.toggleModal(false)

    render() {
        return (
            <React.Fragment>
                <Modal
                    onRequestClose={this.close}
                    isOpen={this.props.isOpen}
                    style={S.Container(this.props.transparent, this.props.stickBottom)}
                >
                    <S.Frame>
                        {/*{!this.props.withOutCross && <S.Close transparent={this.props.transparent}*/}
                        {/*                                      onClick={() => this.props.toggleModal(false)}>&#x2715;</S.Close>}*/}
                        <S.Content>
                            <S.ModalHeader>
                                <S.Title fontSize={6}>{this.props.title}</S.Title>
                                <BtnCloseModal close={this.close} />
                            </S.ModalHeader>
                            {this.props.children}</S.Content>
                    </S.Frame>
                </Modal>
            </React.Fragment>
        );
    }
}

export default ModalWrapper;
