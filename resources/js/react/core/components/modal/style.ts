import styled from 'styled-components';
import React, { CSSProperties } from 'react'

interface IProps {
    fontSize: number;
}

export const Container = (transparent: boolean, stickBottom: boolean) => {
    const content = {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        // width: '70%',
        padding: '10px',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderRadius: '15px',
        overlfow: 'auto',
        overlfowX: 'hidden',
        border: '1px solid rgb(204, 204, 204)',
        background: 'rgb(255, 255, 255)',
        minWidth: '460px'
    };
    transparent && (content.border = 'none');
    transparent && (content.background = 'none');
    stickBottom && (content.top = 'auto');
    stickBottom && (content.bottom = '0px');
    stickBottom && (content.transform = 'translate(-50%, -10%)');

    return {
        content: content,
        overlay: {
            zIndex: 10000,
            backgroundColor: 'rgba(53, 53, 53, 0.76)',
            overflow: 'auto'
        }

    }
        ;
};

export const Content = styled.div`
    padding: 0px;
    max-width: 1000px;
    height:100%;
    //@media (max-width: 1280px) {
    //    max-width: 1000px;
    //}
    //@media (max-width: 854px) {
    //    max-width: 600px;
    //}
    //@media (max-width: 640px) {
    //    max-width: 600px;
    //}
`;

export const Close = styled.div<{ transparent?: boolean }>`
    font-size: 36px;
    color: ${({transparent}) => transparent ? '#000' : '#d0d0d0'};
    cursor: pointer;
    margin: 12px 8px;
    position: absolute;
    right: 0;
`;

export const ModalHeader = styled.div`
	display: -ms-flexbox;
	display: flex;
	-ms-flex-align: start;
	align-items: flex-start;
	-ms-flex-pack: justify;
	justify-content: space-between;
	border-bottom: 1px solid #dee2e6;
	border-top-left-radius: 0.3rem;
	border-top-right-radius: 0.3rem;
	padding-bottom: 8px;
`;

export const Title = styled.div<IProps>`
    margin-left: 6px;
	font-weight: 600;
	font-size: ${props => 2.5 - 0.25 * props.fontSize}rem;
`;

export const Frame = styled.div`
`;
