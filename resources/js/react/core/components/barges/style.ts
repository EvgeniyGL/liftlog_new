import styled from "styled-components";

type Color = 'warning'|'danger'

export const Badge = styled.span.attrs<{color:Color}>({
    className: `badge`
})`
    background-color:${({color})=>color};
`
