import React from "react";
import ReactPaginate from 'react-paginate';

interface IProps {
	handlePageClick: (data) => void
	pageCount: number
	currentPage: number
}

export const Paginate = function (props: IProps) {
	return props.pageCount > 1 && <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        breakClassName={'page-link'}
        pageCount={props.pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={props.handlePageClick}
        containerClassName={'pagination justify-content-end'}
        subContainerClassName={'pages pagination'}
        activeClassName={'active'}
        pageClassName={'page-item'}
        pageLinkClassName={'page-link '}
        previousLinkClassName={'page-link'}
        previousClassName={`page-item ${props.currentPage <= 1 ? 'disabled' : ''}`}
        nextLinkClassName={'page-link'}
        nextClassName={`page-item ${props.currentPage >= props.pageCount ? 'disabled' : ''}`}
        disableInitialCallback={false}
        forcePage={props.currentPage}
    />
}

