import styled from "styled-components";

export const ButtonPrimary = styled.button.attrs({
    className: 'btn btn-primary'
})`
    margin: 0 2px;
    `

export const ButtonDanger = styled.button.attrs({
    className: 'btn btn-danger'
})`
    margin: 0 2px;
    `

export const ButtonSuccess = styled.button.attrs({
    className: 'btn btn-success'
})`
    margin: 0 2px;
    `

export const ButtonPrimarySmall = styled.button.attrs({
    className: 'btn btn-primary btn-sm'
})`
    line-height: 1;
`

export const ButtonSuccessSmall = styled.button.attrs({
    className: 'btn btn-success btn-sm'
})``

export const ButtonOutlineDangerSmall = styled.button.attrs({
    className: 'btn btn-outline-danger btn-xs'
})`    font-size: 12px;
        white-space: nowrap;
        padding: 0 4px;
`

