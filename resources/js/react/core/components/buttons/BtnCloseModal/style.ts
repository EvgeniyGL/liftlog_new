import styled from 'styled-components';

export const Container = styled.button`
    background-color: transparent;
    border: 0;
    color: var(--gray);
    opacity: .5;
    &:hover,&:focus{
        color: #000;
        text-decoration: none;
        opacity: .8;
    }
`
