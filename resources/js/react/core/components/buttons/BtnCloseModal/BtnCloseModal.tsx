import React from 'react';
import * as S from './style';
import {Cross} from 'styled-icons/icomoon';

interface IProps {
    close: () => void
}

export const BtnCloseModal = ({close}: IProps) => (
    <S.Container onClick={() => close()}>
        <Cross width={15} height={15}/>
    </S.Container>
);
