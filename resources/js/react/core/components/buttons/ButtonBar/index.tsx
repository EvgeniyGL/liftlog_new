import React from 'react';
import {inject, observer} from 'mobx-react';
import * as S from './style';
import {ButtonDanger, ButtonPrimary} from "@core/components/buttons/style";

interface IProps {
    add: () => void
    edit: () => void
    remove: () => void
    search?: () => void
    resetSearch?: () => void
    extra?: React.Component
}

@observer
class ButtonBar extends React.Component<IProps> {

    public render() {
        const {search, extra, add, edit, remove, resetSearch} = this.props

        return (
            <S.Container id="btn-group-action">
                <S.ButtonGroup>
                    <ButtonPrimary onClick={add}>Добавить</ButtonPrimary>
                    <ButtonPrimary onClick={edit}>Изменить</ButtonPrimary>
                    <ButtonPrimary onClick={remove}>Удалить</ButtonPrimary>
                </S.ButtonGroup>
                {search && <S.ButtonGroup>
                    {resetSearch && <ButtonDanger onClick={resetSearch}>Сбросить поиск</ButtonDanger>}
                    <ButtonPrimary onClick={search}>Поиск</ButtonPrimary>
                </S.ButtonGroup>}
                {extra && extra}
            </S.Container>
        );
    }
}

export default ButtonBar;
