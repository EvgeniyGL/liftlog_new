import styled from 'styled-components';

const LoaderContainer = styled.div`
    display: flex;
    position: fixed;
    left: 0;
    top: 0;
    background-color: rgba(215,215,215,0.5);
    width: 100%;
    height: 100%;
    justify-content: center;
    z-index: 10000!important;
  `

export default LoaderContainer
