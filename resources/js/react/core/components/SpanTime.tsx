import React from 'react';

interface IProps {
    dateString: string;
}

const getTime = (date: string) => {
    const options = {hour: '2-digit', minute: '2-digit'};
    const time = new Date(date).toLocaleTimeString('ru-RU', options);
    return time;
};

const SpanTime: React.FC<IProps> = props =>
    !props.dateString ? null : <span>{getTime(props.dateString)}</span>;
export default SpanTime;
