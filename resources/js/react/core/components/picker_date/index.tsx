import * as React from 'react';
import DatePicker from 'react-datepicker'
import * as S from './style';
import { registerLocale } from "react-datepicker";
import ru from 'date-fns/locale/ru';
import moment from "moment";
import Autocomplete from "react-autocomplete";
import {FORMAT_DATE_TIME} from "@react/Root/store";
registerLocale('ru', ru)

interface IProps {
    initialDate: Date|string
    setFieldValue: (field: string, val: string) => void;
    placeholder: string
    name: string
    label?: string
    error?: string;
    onBlur?: ({target: {value:string}}) => void
}

const  setDate=(setFieldValue, date:Date, name:string)=>{
    if(date===null) return;
    const val = moment(date).format(FORMAT_DATE_TIME)
    setFieldValue(name, val)
}

const DatePickerInput: React.FunctionComponent<IProps> = (props) => {
    return <React.Fragment>
        <S.Container error={props.error}>
            {props.label && <S.Label>{props.label}: </S.Label>}
            <DatePicker className='filters__date-choose'
                        title={'Формат даты должен быть 29.09.2019 18:16'}
                        selected={props.initialDate}
                        onChange={date => setDate(props.setFieldValue, date, props.name)}
                // onSelect={date => props.setStartDate(date)}
                        startDate={props.initialDate}
                // endDate={props.endDate}
                        locale="ru"
                        dateFormat="dd.MM.yyyy HH:mm"
                        placeholderText={props.placeholder}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={5}
            />
            <S.Error>{props.error}</S.Error>
        </S.Container>
    </React.Fragment>
}



export default DatePickerInput;
