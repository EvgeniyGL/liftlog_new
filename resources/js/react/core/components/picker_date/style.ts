import styled from 'styled-components';

export const Container = styled.div<{ error: string }>`
    ${({error}) => error && `&.filters__date-choose{
        color:'red';
        }`
}`;

export const Label = styled.label.attrs({
    className: 'form-check-label',
})``;

export const Error = styled.div.attrs({
    className: 'text-danger',
})`
	font-size: 12px;
	width: 100%;
`;
