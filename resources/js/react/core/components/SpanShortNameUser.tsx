import React from 'react';

interface IProps {
  user: {
    name: string;
  };
}

const getShortName = (name: string) => {
  const arrName = name.split(' ');
  let shortName = arrName[0];
  shortName += !arrName[1] ? '' : ' ' + arrName[1][0] + '. ';
  shortName += !arrName[2] ? '' : ' ' + arrName[2][0] + '.';
  return shortName;
};

const SpanShortNameUser: React.FC<IProps> = props =>
  !props.user ? null : (
    <span title={props.user.name}>
      {getShortName(!!props.user && props.user.name)}
    </span>
  );
export default SpanShortNameUser;
