import Modal from 'styled-react-modal';
import styled from 'styled-components';

export const ModalForm = Modal.styled`
  z-index: 300000;    
  position: absolute;
  top: 10%;
`;
export const ModalContent = styled.div`
	position: relative;
	display: -ms-flexbox;
	display: flex;
	-ms-flex-direction: column;
	flex-direction: column;
	pointer-events: auto;
	background-color: #fff;
	background-clip: padding-box;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 0.3rem;
	outline: 0;
`;

export const ModalHeader = styled.div`
	display: -ms-flexbox;
	display: flex;
	-ms-flex-align: start;
	align-items: flex-start;
	-ms-flex-pack: justify;
	justify-content: space-between;
	padding: 0rem 1rem;
	border-bottom: 1px solid #dee2e6;
	border-top-left-radius: 0.3rem;
	border-top-right-radius: 0.3rem;
`;
export const ModalBody = styled.div`
	position: relative;
	-ms-flex: 1 1 auto;
	flex: 1 1 auto;
	padding: 1rem;
`;

export const modalFooter = styled.div`
	display: -ms-flexbox;
	display: flex;
	-ms-flex-align: center;
	align-items: center;
	-ms-flex-pack: end;
	justify-content: flex-end;
	padding: 1rem;
	border-top: 1px solid #dee2e6;
	border-bottom-right-radius: 0.3rem;
	border-bottom-left-radius: 0.3rem;
`;

interface IProps {
    fontSize: number;
}

export const Title = styled.div<IProps>`
	margin-bottom: 0;
	line-height: 1.5;
	font-weight: 500;
	margin-top: 0;
	font-size: ${props => 2.5 - 0.25 * props.fontSize}rem;
	float: left;
`;
