import React from 'react';
import * as S from './style';
import {BtnCloseModal} from '@core/components/buttons/BtnCloseModal/BtnCloseModal';

interface IProps {
    isOpen: boolean;
    toggleModal?: (show: boolean) => void;
    title: string;
}

const Modal: React.FC<IProps> = props => (
    <S.ModalForm isOpen={props.isOpen}
        // onBackgroundClick={props.toggleModal}
                 onEscapeKeydown={props.toggleModal}
    >
        <S.ModalContent>
            <S.ModalHeader>
                <S.Title fontSize={5}>{props.title}</S.Title>
                <BtnCloseModal close={() => this.props.close(false)}/>
            </S.ModalHeader>
            <S.ModalBody>{props.children}</S.ModalBody>
        </S.ModalContent>
    </S.ModalForm>
);
export default Modal;
