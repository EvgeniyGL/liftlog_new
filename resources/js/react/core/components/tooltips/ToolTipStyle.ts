import styled from "styled-components";

export const ToolTipText = styled("span")({
    visibility: "hidden",
    width: "180px",
    fontSize: "12px",
    backgroundColor: "#000",
    color: "#fff",
    textAlign: "center",
    borderRadius: "6px",
    padding: "5px 0",
    position: "absolute",
    zIndex: 1,
    bottom: "100%",
    left: "50%",
    whiteSpace: "initial",
    marginLeft: "-90px",
    lineHeight: '0.9em',
    ":after": {
        content: '""',
        position: "absolute",
        top: "100%",
        left: "50%",
        marginLeft: "-5px",
        borderWidth: "5px",
        borderStyle: "solid",
        borderColor: "black transparent transparent transparent"
    }
});

export const Tip = styled("div")({
    position: "relative",
    display: "inline-block",
    borderBottom: "1px dotted black",
    ":hover span": {
        visibility: "visible"
    }
});
