import React from "react";
import {Tip, ToolTipText} from "@core/components/tooltips/ToolTipStyle";

interface IProps {
    children: React.ReactElement;
    toolTipText: string;
}

export const ToolTip = (props: IProps) => (
    <Tip>
        {props.children}
        <ToolTipText>{props.toolTipText}</ToolTipText>
    </Tip>
);

