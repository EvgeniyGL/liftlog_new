import ArrowDown from './ArrorDown';
import ArrowUp from './ArrowUp';
import Loader from './Loader';

/**
 * @export Icons
 * @default
 */
export default {
	ArrowDown,
	ArrowUp,
	Loader,
};
