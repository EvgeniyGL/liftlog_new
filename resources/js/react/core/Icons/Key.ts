import styled from 'styled-components';
import {Key as FaKey} from "styled-icons/fa-solid";

export const Key = styled(FaKey)<{color?:string}>`
      height: 14px;
      margin-left: 5px;
      color: var(--green);
`
