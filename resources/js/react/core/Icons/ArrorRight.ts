import styled from 'styled-components';
import {RightArrow as Arrow} from 'styled-icons/boxicons-solid/RightArrow'

const RightArrow = styled(Arrow)`
    color: #007bff;
    font-size: 0.7em;
    width: 3em;
    &:hover,
	:focus {
		cursor: pointer;
        color:#0069d9,
	}
	&:active {
		transform: translate(1px, 1px);
		filter: saturate(150%);
	}
    `

export default RightArrow