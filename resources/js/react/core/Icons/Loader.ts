import styled, { keyframes } from 'styled-components';
import { LoaderAlt } from 'styled-icons/boxicons-regular/LoaderAlt'

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Loader = styled(LoaderAlt)`
color: #dee2e6;
width:4em;
animation: ${rotate} 2s linear infinite;
`

export default Loader