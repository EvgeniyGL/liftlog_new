import styled from 'styled-components';

const ArrowUp = styled.div`
	color: #007bff;
	font-size: 0.5em;
	width: 4em;
	&:hover {
		color: #0069d9;
	}
`;

export default ArrowUp;
