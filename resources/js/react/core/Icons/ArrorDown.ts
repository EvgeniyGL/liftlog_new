import styled from 'styled-components';
import {DownArrowSquare} from 'styled-icons/boxicons-solid/DownArrowSquare'

const ArrowDown = styled(DownArrowSquare)`
color: #007bff;
    font-size: 0.5em;
    width:4em;
    &:hover{
        color:#0069d9
    }`

export default ArrowDown