import {computed, observable} from 'mobx';

export interface IRole {
    id: number;
    name: string;
    permissions: IPermission[];
}

export interface IPermission {
    id: number;
    name: string;
}


export interface IUser {
    id: number;
    name: string;
    email: string;
    roles: IRole[];
    phone: number;
}

export interface IOption {
    value: string;
    label: string;
}

export interface ICurrentUser {
    id: number,
    name: string,
    role: string
}

export interface IRecord {
    id: string;
    num: string;
    creator_id: number;
    time_create: string;
    type: string;
    theme: string;
    time_sent: string;
    maker_id: number;
    time_take: string;
    time_done: string;
    closer_id: number;
    theme_end: string;
    notice: string;
    time_incident: string;
    evacuation: '1' | '0';
    time_evacuation: string;
    addresses_id: string[];
    addresses: IAddress[];
}

export interface IAddress {
    address_id: string
    address_name: string
}

export interface IAddresses {
    [i: number]: IAddress;
}


export interface IResponse<Data = any> {
    status: boolean
    code: number
    message: string
    data: Data
}

export const FORMAT_DATE_TIME = 'DD.MM.YYYY HH:mm';

export default class RootStore {

    private readonly listTypes = window.sharedData.types;

    @observable
    public optionsMaker: IOption[] = [];
    @observable
    public readonly currentUser: ICurrentUser = window.sharedData.currentUser;

    @computed get recordTypes(): string[] {
        return this.listTypes.map(type => type.title).sort();
    };

    @computed get recordTypesOption(): IOption[] {
        return this.listTypes.map(type => ({
            value: type.id,
            label: type.title
        }));
    };

    getOptionsMaker = (users: IUser[]): void => {
        this.optionsMaker = Object.keys(users).map((key: any) => {
            return {value: `${users[key].id}`, label: users[key].name};
        });
    };

}
