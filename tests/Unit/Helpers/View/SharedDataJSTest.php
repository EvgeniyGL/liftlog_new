<?php

namespace Tests\Unit\Helpers\View;

use App\Helpers\View\SharedDataJS;
use Coderello\SharedData\Facades\SharedData;
use Tests\TestCase;

class SharedDataJSTest extends TestCase
{

    public function testPutDataByName(): void
    {
        $data = [
            'id' => 17,
        ];
        $SharedDataJS = new SharedDataJS();
        $SharedDataJS->put(
            $data,
            "test"
        );

        self::assertEquals(shared()->get('test'), $data);
    }

    public function testPutArrayData(): void
    {
        $data = [
            'id' => 17,
        ];
        $SharedDataJS = new SharedDataJS();
        $SharedDataJS->put(
            $data
        );
        self::assertEquals(shared()->get('id'), $data['id']);
    }
}
