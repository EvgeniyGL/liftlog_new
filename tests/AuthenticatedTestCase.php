<?php

namespace Tests;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Auth;

abstract class AuthenticatedTestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected $user;

    protected function createAndLoginAdmin()
    {
        $this->user = User::factory()->create(['password' => 'secret']);
        $role = new Role();
        $role->name = 'администратор';
        $role->slug = 'admin';
        $role->save();
        $admin = Role::where('slug','admin')->first();
        $this->user->roles()->attach($admin);

        $this->actingAs($this->user);
    }

}
