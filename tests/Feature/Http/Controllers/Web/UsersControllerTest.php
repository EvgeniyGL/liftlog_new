<?php

namespace Tests\Feature\Http\Controllers\Web;

use Tests\AuthenticatedTestCase;

class UsersControllerTest extends AuthenticatedTestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $this->createAndLoginAdmin();
        $response = $this->get('/users');
        $response->assertStatus(200);
        $shared = shared()->toArray();
        self::assertArrayHasKey('users', $shared);
        self::assertArrayHasKey('roles', $shared);
    }
}
