<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Web\UsersController;
use Illuminate\Support\Facades\Route;

//Route::get('/', [AuthenticatedSessionController::class, 'create'])
//                ->middleware('guest')
//                ->name('login');

Route::group(
    [
        'prefix'     => 'users',
        'middleware' => ['auth']
    ],
    function () {
        Route::get('/', [UsersController::class, 'index'])->name('users');
        Route::post('/destroy/{id}', [UsersController::class,'destroy']);
        Route::post('/access', [UsersController::class,'access']);
        Route::post('/address', [UsersController::class,'address']);
        Route::post('/create', [UsersController::class,'create'])->name('users.create');
        Route::get('/token', [UsersController::class,'token']);
        Route::post('/set_notificate/{userId}', [UsersController::class,'setNotificate']);
    }
);
