FROM php:7.4-fpm-alpine

ADD ./docker/php/www.conf /usr/local/etc/php-fpm.d/

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel -u 1001

RUN mkdir -p /var/www/html

RUN chown -R laravel:laravel /var/www/html

WORKDIR /var/www/html

RUN apk update

RUN set -ex; \
    \
    apk add --no-cache \
    autoconf \
    g++ \
    make \
    postgresql-dev

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \

    # Finally, install php compiled extensions
    && docker-php-ext-install \
        pdo \
        pdo_pgsql \
        pgsql \
        tokenizer

RUN pecl install redis \
    && docker-php-ext-enable redis
