FROM nginx:1.19.9-alpine

#change default configures on vhost.conf
ADD docker/conf/vhost.conf /etc/nginx/conf.d/default.conf

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel -u 1001

RUN mkdir -p /var/www/html

RUN chown laravel:laravel /var/www/html
#define work directory, when we go inside docker this folder will open
WORKDIR /var/www/html
